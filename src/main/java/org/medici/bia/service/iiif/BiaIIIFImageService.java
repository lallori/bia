package org.medici.bia.service.iiif;

import org.medici.bia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface BiaIIIFImageService {

	public String findImage(Integer imageId) throws ApplicationThrowable;

}
