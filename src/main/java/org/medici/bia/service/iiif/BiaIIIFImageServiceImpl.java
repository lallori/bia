package org.medici.bia.service.iiif;

import org.medici.bia.dao.image.ImageDAO;
import org.medici.bia.domain.Image;
import org.medici.bia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
@Transactional(readOnly = true)
public class BiaIIIFImageServiceImpl implements BiaIIIFImageService {

	@Autowired
	private ImageDAO imageDAO;

	@Override
	public String findImage(Integer imageId) throws ApplicationThrowable {
		try {
			Image image= getImageDAO().find(imageId);
			if(image !=null){
				return image.toString();
			}
			
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		
		return null;
	}

	public ImageDAO getImageDAO() {
		return imageDAO;
	}

	public void setImageDAO(ImageDAO imageDAO) {
		this.imageDAO = imageDAO;
	}

}