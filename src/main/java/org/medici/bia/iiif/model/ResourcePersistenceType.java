package org.medici.bia.iiif.model;

public enum ResourcePersistenceType {
  REFERENCED, MANAGED, RESOLVED, CUSTOM
}
