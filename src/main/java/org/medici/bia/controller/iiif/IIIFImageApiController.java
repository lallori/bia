package org.medici.bia.controller.iiif;

import java.io.File;
import java.io.FileInputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.medici.bia.common.util.DateUtils;
import org.medici.bia.iiif.model.*;
import org.medici.bia.service.iiif.IIIFImageService;
import org.medici.bia.service.iiif.IIIFImageServiceImpl;
import org.medici.bia.service.iiif.IIIFParameterParserService;
import org.medici.bia.service.iiif.IIIFParameterParserServiceImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/iiif/getIIIFImage/")
public class IIIFImageApiController {

	private final Logger logger = Logger.getLogger(this.getClass());

	// @Autowired
	// private MiaIIIFImageService MiaIIIFImageService;
	// http://localhost:8181/DocSources/json/iiif/getIIIFImage/155/full/full/0/default.jpg

	// http://localhost:8181/Mia/json/iiif/getIIIFImage/155/full/full/0/default.jpg
	@RequestMapping(value = "{identifier}/{region}/{size}/{rotation}/{quality}.{format}")
	public ResponseEntity<byte[]> getIIIFImage(@PathVariable String identifier,
			@PathVariable String region, @PathVariable String size,
			@PathVariable String rotation, @PathVariable String quality,
			@PathVariable String format, HttpServletRequest request) {

		final HttpHeaders headers = new HttpHeaders();
		ResponseEntity<byte[]> responseEntity = null;
		long start = System.currentTimeMillis();

		try {

			if (identifier == null || identifier.isEmpty()) {
				return null;
			}

			IIIFParameterParserService iiifParameterParserService = new IIIFParameterParserServiceImpl();

			RegionParameters regionParameters = iiifParameterParserService
					.parseIiifRegion(region);

			ResizeParameters sizeParameters = iiifParameterParserService
					.parseIiifSize(size);
			RotationParameters rotationParameters = iiifParameterParserService
					.parseIiifRotation(rotation);
			IIIFImageBitDepth bitDepthParameter = iiifParameterParserService
					.parseIiifQuality(quality);
			IIIFImageFormat formatParameter = iiifParameterParserService
					.parseIiifFormat(format);

			// String filePath = getMiaIIIFImageService().getIIIFImageFileName(
			// Integer.valueOf(identifier));
			 String filePath = "";

			byte[] file = readContentIntoByteArray(filePath);

			IIIFImageService imageService = new IIIFImageServiceImpl();

			IIIFImage image = imageService.processJAIImage(file,
					regionParameters, sizeParameters, rotationParameters,
					bitDepthParameter, formatParameter);
			final IIIFImageFormat imageFormat = image.getFormat();
			final String mimeType = imageFormat.getMimeType();
			headers.setContentType(MediaType.parseMediaType(mimeType));
			byte[] data = image.toByteArray();

			responseEntity = new ResponseEntity<byte[]>(data, headers,
					HttpStatus.CREATED);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		long end = System.currentTimeMillis();
		logger.info("WHOISONLINEJOB ends at "
				+ DateUtils.getMYSQLDateTime(new DateTime(end)));
		logger.info("WHOISONLINEJOB work time: "
				+ (new Float(end - start) / 1000));

		return responseEntity;

	}

	private byte[] readContentIntoByteArray(String filePath) {

		if (filePath == null)
			return null;

		File file = new File("C:/Users/user/Desktop/IIIFImages/iiif.jpg");
		// File file = new File(filePath);
		FileInputStream fileInputStream = null;
		byte[] bFile = new byte[(int) file.length()];
		try {
			// convert file into array of bytes
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bFile;
	}

	// public MiaIIIFImageService getMiaIIIFImageService() {
	// return MiaIIIFImageService;
	// }
	//
	// public void setMiaIIIFImageService(MiaIIIFImageService
	// miaIIIFImageService) {
	// MiaIIIFImageService = miaIIIFImageService;
	// }

}