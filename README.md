# README #

BIA represents the culmination of over twenty years of research and development in historical scholarship and digital humanities. Unlike other digital archives, BIA was designed to innovate and experiment with traditional academic boundaries. As such, BIA is more than a search tool, but a digital portal to an otherwise lost world of early modern Europe from the perspective of Grand Ducal Tuscany. BIA allows for advanced searches into the epistolary collection of the Grand Ducal Medici Family. Users are able to find entire volumes or individual documents by search through place, person, and category tags, archival collocation, date of creation, and keywords (from their Italian transcription and/or English synopsis). Moreover, BIA provides a scholarly community in which to pose questions and ask for help. Pending MAP Staff approval, scholars worldwide can even transcribe and annotate unpublished documents, effectively serving as Distant Fellows (contact any Staff Member for more details). BIA’s influence on Medici Studies has been profound, with numerous publications (articles and monographs) and presentation (conference papers and public lectures) making use of the material presented on BIA. Inside BIA a software module to teach paleography has been developed.


The source code has been released as open source (GNU GPL V2) and is freely available to be used for other archives/projects.

A production version is running at: http://bia.medici.org


![Screen-Shot-2015-10-12-at-18.05.15.png](https://bitbucket.org/repo/e4p5d6/images/495992982-Screen-Shot-2015-10-12-at-18.05.15.png)


The BIA's Teaching module:

![Untitled-1.jpg](https://bitbucket.org/repo/e4p5d6/images/1339886242-Untitled-1.jpg)
 



* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact